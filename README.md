# Dark Wood dokuwiki template

* Designed by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information about this template](http://dokuwiki.org/template:darkwood)

![dark wood dokuwiki theme screenshot](https://i.imgur.com/snoXkug.png)
